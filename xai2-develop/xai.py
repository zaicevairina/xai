import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from catboost import CatBoostClassifier,Pool
import pickle
import shap
import numpy as np
import pandas as pd

from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from lime.lime_tabular import LimeTabularExplainer
from functools import partial

def explain_one_tree_shap(name,num_tree):
    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)    
    
    model_one_tree = model.copy()
    model_one_tree.shrink(ntree_start = num_tree,ntree_end=num_tree+1)
    x_train = df
    feature_names = list(df.columns)[:-1]
    explainer = shap.TreeExplainer(model_one_tree)
    shap_values = explainer.shap_values(x_train)
    shap_values = shap.TreeExplainer(model_one_tree).shap_values(x_train)
    importence = shap_values.mean(axis=0)
    y = zip( feature_names,importence)
    xs = sorted(y, key=lambda tup: abs(tup[1]))[::-1]
    return xs


def convert_to_lime_format(X, categorical_names, col_names=None, invert=False):

    if not isinstance(X, pd.DataFrame):
        X_lime = pd.DataFrame(X, columns=col_names)
    else:
        X_lime = X.copy()

    for k, v in categorical_names.items():
        if not invert:
            label_map = {str_label: int_label for int_label, str_label in enumerate(v)}
        else:
            label_map = {int_label: str_label for int_label, str_label in enumerate(v)}

        X_lime.iloc[:, k] = X_lime.iloc[:, k].map(label_map)

    return X_lime

def custom_predict_proba(X, model):
    cat_features = model._get_params()['cat_features']

    feature_names = ['gender','age','hypertension','heart_disease','ever_married','work_type','Residence_type','avg_glucose_level','bmi','smoking_status']
    categorical_names = {0: ['Male', 'Female', 'Other'],
       2: [0, 1],
       3: [1, 0],
       4: ['Yes', 'No'],
       5: ['Private', 'Self-employed', 'Govt_job', 'children', 'Never_worked'],
       6: ['Urban', 'Rural'],
       9: ['formerly smoked', 'never smoked', 'smokes', 'Unknown']}
    X_str = convert_to_lime_format(X, categorical_names,col_names=feature_names, invert=True)
    return model.predict_proba(X_str)


def lime_func(observation, name):

    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)
    cat_features = model._get_params()['cat_features']
    feature_names = list(df.columns)[:-1]

    num_features = list(set(feature_names).difference(set(cat_features)))
    cat_features_index = []
    for i,j in enumerate(feature_names):
        if j in  cat_features:
            cat_features_index.append(i)     

    
    categorical_names = {}
    for i,j in enumerate(df.columns):
        if j in (cat_features):
            categorical_names[i] = list(df[j].unique())

    predict_proba = partial(custom_predict_proba, model=model)
    observation = pd.DataFrame([list(observation)],columns=feature_names)
    x_train = df.iloc[:,:-1]

    explainer = LimeTabularExplainer(convert_to_lime_format(x_train, categorical_names).values ,
        mode="classification",
        feature_names=feature_names,
        categorical_names=categorical_names,
        categorical_features=cat_features_index,
        discretize_continuous=False,
        random_state=42)
    explanation = explainer.explain_instance(convert_to_lime_format(observation,categorical_names).values[0],
        predict_proba,num_samples=10000,labels=(1,))

    html = explanation.as_html(show_table=True)

    with open(f'media/{name}_lime.html', 'w') as f:
        f.write(html)

    return f'/media/{name}_lime.html'


def fit_model(name,path,params):
        df = pd.read_csv(path)
        y = df.target
        X = df.drop('target', axis=1)
        model = CatBoostClassifier()
        params['train_dir']=name
        model.set_params(**params)
        feature_names = list(X.columns)
        
        pool = Pool(X[:3500], y[:3500], cat_features=params['cat_features'], feature_names=feature_names)
        pool_test = Pool(X[3501:], y[3501:], cat_features=params['cat_features'], feature_names=feature_names)
        model.fit(pool,verbose=False,eval_set= pool_test)

        with open(f'{name}/{name}.pickle', 'wb') as f:
            pickle.dump(model, f)

        with open(f'{name}/{name}_data.pickle', 'wb') as f:
            pickle.dump(df, f)

        s = 'Число деревьев: ' + str(model.tree_count_)

        return (s)

def predict(name,path,obj=None):


     with open(f'{name}/{name}.pickle', 'rb') as f:
         model = pickle.load(f)

     with open(f'{name}/{name}_data.pickle', 'rb') as f:
         df = pickle.load(f)

     #Probability
     Probability = model.predict(obj,prediction_type='Probability')
     #Class
     Class = model.predict(obj,prediction_type='Class')
     #RawFormulaVal
     RawFormulaVal = model.predict(obj,prediction_type='RawFormulaVal')

     return (Probability,Class,RawFormulaVal)





def predict_some_trees(name,ntree_start,ntree_end,obj):
     with open(f'{name}/{name}.pickle', 'rb') as f:
         model = pickle.load(f)
     print('xai',obj)
     print('xai',type(obj))



     #Probability
     Probability = model.predict(obj,prediction_type='Probability',ntree_start=ntree_start,ntree_end=ntree_end)
     #Class
     Class = model.predict(obj,prediction_type='Class',ntree_start=ntree_start,ntree_end=ntree_end)
     #RawFormulaVal
     RawFormulaVal = model.predict(obj,prediction_type='RawFormulaVal',ntree_start=ntree_start,ntree_end=ntree_end)

     return (Probability,Class,RawFormulaVal)




def show_tree(name,id):
    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)


    feature_names = list(df.columns)[:-1].extend('tatget')
    cat_features = model._get_params()['cat_features']
    X = df
    pool_tr = Pool(X, cat_features=cat_features, feature_names=feature_names)
    model.plot_tree(tree_idx=id,pool=pool_tr,path=f'media/{name}_tree_{id}')
    path = f'/media/{name}_tree_{id}.png'
    return path

def important_trees(name,obj):
    data_tree_obj = []
    
    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    for tree in range(1,model._tree_count-2):
        RawFormulaVal_obj = 0
        y_pred = model.predict(obj,ntree_start=tree,ntree_end=tree+1)
        RawFormulaVal = model.predict(obj,ntree_start=tree,ntree_end=tree+1,prediction_type='RawFormulaVal')

        data_tree_obj.append([tree,RawFormulaVal])
    df_obj = pd.DataFrame(data_tree_obj,columns=['tree','val'])
    df_obj['abs'] = abs(df_obj['val'])
    return ('Деревья вносящие наибольший вклад в предсказания: '+str(list(df_obj.sort_values(['abs'])['tree'])[::-1][:5])[1:-1])



def XAI(name,path,obj=None):



    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)


    shap.initjs()
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values([obj])
    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)

    feature_names = list(df.columns)
    feature_names.remove('target')

    shap.force_plot(explainer.expected_value, shap_values[0,:],
                       features=np.array(obj),feature_names=feature_names)

    shap.save_html(f'media/{name}'+'_shap_1.html', shap.force_plot(explainer.expected_value, shap_values[0,:],
                   features=np.array(obj),feature_names=feature_names))
    path = f'/media/{name}'+'_shap_1.html'
    return path

def info(name):

     with open(f'{name}/{name}.pickle', 'rb') as f:
         model = pickle.load(f)

     with open(f'{name}/{name}_data.pickle', 'rb') as f:
         df = pickle.load(f)

     count_tree = 'Число деревьев в ансамбле '+ str(model.tree_count_)
     train_size = 'Размер обучающей выборки ' + str(df.shape[0])

     y = df.target
     X = df.drop('target', axis=1)

     feature_names = list(df.columns)[:-1].extend('tatget')
     cat_features = model._get_params()['cat_features']

     y = df.target
     X = df.drop('target', axis=1)

     pool_tr = Pool(X,y ,cat_features=cat_features, feature_names=feature_names)
     feature_importance_PredictionValuesChange = model.get_feature_importance(pool_tr,type='PredictionValuesChange',prettified=True)

     feature_importance_LossFunctionChange = model.get_feature_importance(pool_tr,type='LossFunctionChange',prettified=True)


     x = model.get_params()
     params = []
     for param, value in x.items():
         if param!='train_dir':
            if isinstance(value,list):
              s = param + ': ' + ', '.join(value)
            else:
              s = param + ': ' + str(value)
            params.append(s)



     feature_names = list(df.columns)
     feature_names.remove('target')
     feature_names = 'Признаки использующиеся в обучении ' +','.join(feature_names)


     return (feature_names,count_tree,train_size,feature_importance_PredictionValuesChange,feature_importance_LossFunctionChange,params)
    #     описание модели


    #     график

def interaction(name):
     with open(f'{name}/{name}.pickle', 'rb') as f:
         model = pickle.load(f)

     with open(f'{name}/{name}_data.pickle', 'rb') as f:
         df = pickle.load(f)
     y = df.target
     X = df.drop('target', axis=1)

     feature_names = list(df.columns)[:-1].extend('tatget')
     cat_features = model._get_params()['cat_features']

     pool_tr = Pool(X,y ,cat_features=cat_features, feature_names=feature_names)

     fi = model.get_feature_importance(pool_tr, type="Interaction")

     fi_new = []
     for k,item in enumerate(fi):
         first = X.dtypes.index[fi[k][0]]
         second = X.dtypes.index[fi[k][1]]
         if first != second:
            fi_new.append([first + "_" + second, fi[k][2]])

     feature_score = pd.DataFrame(fi_new,columns=['Feature-Pair','Score'])
     feature_score = feature_score.sort_values(by='Score', ascending=False, inplace=False, kind='quicksort', na_position='last')
     plt.rcParams["figure.figsize"] = (16,7)
     ax = feature_score.plot('Feature-Pair', 'Score', kind='bar', color='c')
     ax.set_title("Pairwise Feature Importance", fontsize = 14)
     ax.set_xlabel("features Pair")
     path = (f'/media/{name}_pair.png')
     plt.savefig(f'media/{name}_pair.png', format='png', bbox_inches = 'tight')
     plt.show()
     plt.close()
     return path

def shap_summary(name):

     with open(f'{name}/{name}.pickle', 'rb') as f:
         model = pickle.load(f)

     with open(f'{name}/{name}_data.pickle', 'rb') as f:
         df = pickle.load(f)

     del df['target']
     explainer = shap.TreeExplainer(model)

     observations = df.to_numpy()
     shap_values = explainer.shap_values(observations)

     feature_names = list(df.columns)

     shap.summary_plot(shap_values, features=observations, feature_names=feature_names, show=False)

     path = f'/media/{name}_shap_summary.png'
     plt.savefig(f'media/{name}_shap_summary.png', format='png', bbox_inches = 'tight')
     plt.close()
     return path

def shap_importance(name):
    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)

    del df['target']
    explainer = shap.TreeExplainer(model)

    observations = df.to_numpy()
    shap_values = explainer.shap_values(observations)

    feature_names = list(df.columns)
    shap_values = shap.TreeExplainer(model).shap_values(observations)
    x = abs(shap_values)
    importence = x.mean(axis=0)
    y = zip(importence, feature_names)

    xs = sorted(y, key=lambda tup: tup[0])[::-1]
    return xs

def fit_model_cv(name,path,params,num_folds):
    df = pd.read_csv(path)
    y = df.target
    X = df.drop('target', axis=1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
    model = CatBoostClassifier()
    params['train_dir']=name
    model.set_params(**params)
    feature_names = list(X.columns)
    pool = Pool(X_train, y_train, cat_features=params['cat_features'], feature_names=feature_names)
    pool_valid = Pool(X_test, y_test, cat_features=params['cat_features'], feature_names=feature_names)

    model.fit(pool,verbose=False,eval_set=pool_valid)


    with open(f'{name}/{name}.pickle', 'wb') as f:
        pickle.dump(model, f)
    with open(f'{name}/{name}_data.pickle', 'wb') as f:
        pickle.dump(df, f)


    cv_data = np.array(X.values.tolist())
    labels = np.array(y.values.tolist())
    kfold = KFold(num_folds, True, 1)
    i = 0
    for train, test in kfold.split(cv_data):
        i+=1
        model = CatBoostClassifier()
        params['train_dir']=name+'_cv_'+str(i)
        model.set_params(**params)

        pool = Pool(cv_data[train], labels[train])
        pool_valid = Pool(cv_data[test], labels[test], cat_features=params['cat_features'], feature_names=feature_names)
#         model.fit(pool,verbose=False)
        model.fit(pool,verbose=False,eval_set=pool_valid)
        with open(f'{name}/{name}_cv_{i}.pickle', 'wb') as f:
            pickle.dump(model, f)

        with open(f'{name}/{name}_cv_data_{i}.pickle', 'wb') as f:
            pickle.dump(test, f)

        with open(f'{name}/{name}_cv_df_{i}.pickle', 'wb') as f:
            pickle.dump(df, f)

def shap_summary_cv(name):
    list_shap_values=[]
    list_observations=[]

    for i in range(1,6):
        with open(f'{name}/{name}_cv_{str(i)}.pickle', 'rb') as f:
            model = pickle.load(f)

        with open(f'{name}/{name}_cv_df_{i}.pickle', 'rb') as f:
            df = pickle.load(f)

        del df['target']
        explainer = shap.TreeExplainer(model)

        list_observations.extend(df.to_numpy())
        observations=df.to_numpy()
        list_shap_values.extend(explainer.shap_values(observations))

        feature_names = list(df.columns)

#     shap_values = np.array(list_shap_values).reshape(1,-1)
#     observations = np.array(list_observations).reshape(1,-1)
    shap_values = np.array(list_shap_values)
    observations = np.array(list_observations)
    shap.summary_plot(shap_values, features=observations, feature_names=feature_names, show=False)

    path = f'/media/{name}_shap_summary_cv.png'
    plt.savefig(f'media/{name}_shap_summary_cv.png', format='png', bbox_inches = 'tight')
    plt.close()
    return path

def info_cv(name):

    with open(f'{name}/{name}.pickle', 'rb') as f:
        model = pickle.load(f)

    with open(f'{name}/{name}_data.pickle', 'rb') as f:
        df = pickle.load(f)

    y = df.target
    X = df.drop('target', axis=1)
    feature_names = list(df.columns)[:-1].extend('tatget')
    cat_features = model._get_params()['cat_features']
    y = df.target
    X = df.drop('target', axis=1)
    pool_tr = Pool(X,y ,cat_features=cat_features, feature_names=feature_names)
    feature_importance_PredictionValuesChange = model.get_feature_importance(pool_tr,type='PredictionValuesChange',prettified=True)
    feature_importance_LossFunctionChange = model.get_feature_importance(pool_tr,type='LossFunctionChange',prettified=True)

    list_feature_importance_PredictionValuesChange=[]
    list_feature_importance_LossFunctionChange=[]
    for i in range(1,6):
        with open(f'{name}/{name}_cv_{str(i)}.pickle', 'rb') as f:
            model = pickle.load(f)

        with open(f'{name}/{name}_cv_df_{i}.pickle', 'rb') as f:
            df = pickle.load(f)


        y = df.target
        X = df.drop('target', axis=1)
        feature_names = list(df.columns)[:-1].extend('tatget')
        cat_features = model._get_params()['cat_features']
        y = df.target
        X = df.drop('target', axis=1)
        pool_tr = Pool(X,y ,cat_features=cat_features, feature_names=feature_names)
        list_feature_importance_PredictionValuesChange.extend(model.get_feature_importance(pool_tr,type='PredictionValuesChange',prettified=True).values.tolist())
        list_feature_importance_LossFunctionChange.extend(model.get_feature_importance(pool_tr,type='LossFunctionChange',prettified=True).values.tolist())

    gg = pd.DataFrame(list_feature_importance_LossFunctionChange,columns=['feature','value_cv']).groupby(['feature']).mean().reset_index().sort_values(['value_cv'])
    feature_importance_LossFunctionChange['feature']=feature_importance_LossFunctionChange['Feature Id']
    feature_importance_LossFunctionChange['value_model']=feature_importance_LossFunctionChange['Importances']
    del feature_importance_LossFunctionChange['Feature Id']
    del feature_importance_LossFunctionChange['Importances']
    feature_importance_LossFunctionChange = pd.merge(feature_importance_LossFunctionChange,gg, on=['feature'],how='outer')



    g = pd.DataFrame(list_feature_importance_PredictionValuesChange,columns=['feature','value_cv']).groupby(['feature']).mean().reset_index().sort_values(['value_cv'])
    feature_importance_PredictionValuesChange['feature']=feature_importance_PredictionValuesChange['Feature Id']
    feature_importance_PredictionValuesChange['value_model']=feature_importance_PredictionValuesChange['Importances']
    del feature_importance_PredictionValuesChange['Feature Id']
    del feature_importance_PredictionValuesChange['Importances']
    feature_importance_PredictionValuesChange = pd.merge(feature_importance_PredictionValuesChange,g, on=['feature'],how='outer')

    x = model.get_params()
    params = []
    for param, value in x.items():
        if param!='train_dir':
            if isinstance(value,list):
                s = param + ': ' + ', '.join(value)
            else:
                s = param + ': ' + str(value)
            params.append(s)

    return (feature_importance_PredictionValuesChange,feature_importance_LossFunctionChange, params)
    #     описнаие модели


def shap_importance_cv(name):
    M = shap_importance(name)
    M = pd.DataFrame(M,columns=['value','feature'])
    list_shap_values = []
    feature_names=[]
    for i in range(1,6):
        with open(f'{name}/{name}_cv_{str(i)}.pickle', 'rb') as f:
            model = pickle.load(f)

        with open(f'{name}/{name}_cv_df_{i}.pickle', 'rb') as f:
            df = pickle.load(f)

        del df['target']
        explainer = shap.TreeExplainer(model)
        observations = df.to_numpy()
        list_shap_values.extend(explainer.shap_values(observations))

        feature_names.extend(list(df.columns))
    x = abs(np.array(list_shap_values))
    importence = x.mean(axis=0)
    y = zip(importence, feature_names)
    xs = sorted(y, key=lambda tup: tup[0])[::-1]




    gg = pd.DataFrame(xs,columns=['value_cv','feature']).groupby(['feature']).mean().reset_index().sort_values(['value_cv'],ascending=False)

    imp_shap = pd.merge(M,gg, on=['feature'],how='outer').sort_values(['value'],ascending=False)


    return imp_shap[['feature','value','value_cv']]
