import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from matplotlib.pyplot import table
from .models import LearningModel
from .forms import LearningModelForm
from django.conf import settings
import pandas as pd
import csv
from copy import deepcopy
import json
from django.forms.models import model_to_dict
from  xai import fit_model, info_cv, predict, XAI,predict_some_trees, show_tree, info, interaction, shap_summary, shap_importance, fit_model_cv, shap_summary_cv, shap_importance_cv, lime_func, explain_one_tree_shap,important_trees


ALLOWED_EXTENSIONS = set(['csv'])


@login_required
def get_all_models(request):
    if request.method == 'GET':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).values("id", 'title')
        return render(request, 'learning_models/get_all.html', {'models': learning_models})

@login_required
def compare_models(request):
    if request.method == 'GET':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).values("id", 'title', 'name', 'description')
        models = []
        for model in learning_models:
            res = info(model['name'])[-1]
            model_dict = {'model': model, 'params': res}
            models.append(model_dict)
        return render(request, 'learning_models/compare.html', {'models': models})

@login_required
def get_learn_error(request):
    if request.method == 'GET':
        name = request.GET.get('name', '')

        graph_data = []

        try:
            with open(f'{name}/learn_error.tsv', 'r', newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter='\t')
                for row in reader:
                    graph_data.append(row)

                graph_data = graph_data[1:]

                graph_data_int = []

                for item in graph_data:
                    graph_data_int.append((int(item[0]), float(item[1])))
        except:
            return JsonResponse({ 'error': 'error' })

        graph_data = []
        try:
            with open(f'{name}/test_error.tsv', 'r', newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter='\t')
                for row in reader:
                    graph_data.append(row)

                graph_data = graph_data[1:]

                graph_test_int = []

                for item in graph_data:
                    graph_test_int.append((int(item[0]), float(item[1])))
        except:
            return JsonResponse({ 'error': 'error' })

        return JsonResponse({'graphData': graph_data_int, 'graphDataTest': graph_test_int })

@login_required
# @xframe_options_exempt
@csrf_exempt
def check_model(request, model_id):
    if request.method == 'GET':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).filter(id=model_id).values('id', 'title', 'fields')
        if len(learning_models) == 0:
            return JsonResponse({'nothing': 'nothing'})
        fields = learning_models[0]['fields']
        return render(request, 'learning_models/check_model.html', {'fields': fields})
    elif request.method == 'POST':
        request_fields = request.POST
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).filter(id=model_id)
        if len(learning_models) == 0:
            return JsonResponse({'nothing': 'nothing'})

        for learning_model in learning_models:
            fields = learning_model.fields

            path = learning_model.path
            name = learning_model.name
        value_fields = []
        for field in fields:
            value = request_fields[field]

            if value.isnumeric():
                value = int(value)
            elif value.replace('.', '').replace('-', '').isnumeric():
                value = float(value)
            value_fields.append(value)

        res = predict(name, path, value_fields)

        probability = res[0]
        class1 = res[1]
        raw = res[2]
        iframe_path = XAI(name, path, value_fields)
        iframe_path2 = lime_func(value_fields, name)
        value_fields_json = json.dumps(value_fields)
        string_bla = important_trees(name,value_fields)

        return render(request, 'learning_models/result.html', {
            'probability': probability,
            'class': class1,
            'raw': raw,
            'path': iframe_path,
            'path2': iframe_path2,
            'value_fields': value_fields_json,
            'important_trees': string_bla
        })

@login_required
@csrf_exempt
def create_model(request):
    if request.method == 'GET':
        form = LearningModelForm()
        return render(request, 'learning_models/create_model.html', {'form': form})
    elif request.method == 'POST':

        form = LearningModelForm(request.POST, request.FILES)
        if form.is_valid():
            df = pd.read_csv(form.cleaned_data['model_file'])
            columns = request.POST['columns'].split(',')
            columns.append('target')
            df = df[columns]

            name=request.FILES['model_file'].name
            df.to_csv(str(settings.BASE_DIR) + f'/media/static_csv/{name}', index=False)
            columns = request.POST['columns'].split(',')
            new_model = LearningModel(
                user=request.user,
                path=str(settings.BASE_DIR) + f'/media/static_csv/{name}',
                title=form.cleaned_data['title'],
                name=name,
                description=form.cleaned_data['description'],
                fields = columns,
            )

            new_model.save()

            return JsonResponse({ 'id': new_model.id })
        else:
            return JsonResponse({ 'error': 'error' })

    else:
        HttpResponseNotAllowed(['GET', 'POST'])


@login_required
@csrf_exempt
def create_model2(request):
    if request.method == 'GET':
        form = LearningModelForm()
        return render(request, 'learning_models/create_model.html')
    elif request.method == 'POST':

        form = LearningModelForm(request.POST, request.FILES)
        if form.is_valid():
            df = pd.read_csv(form.cleaned_data['model_file'])
            del df['target']
            names = list(df.columns)
            return JsonResponse({ 'columns': names })
        else:
            return JsonResponse({ 'error': 'error' })
    else:
        HttpResponseNotAllowed(['GET', 'POST'])


@login_required
@csrf_exempt
def learn_model(request, model_id):
    if request.method == 'GET':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).filter(id=model_id)
        if len(learning_models) == 0:
            return HttpResponseRedirect({'nothing': 'nothing'})
        for learning_model in learning_models:
            model = learning_model
            path = learning_model.path

        if model.is_learned:
            return HttpResponseRedirect('/models/')

        df = pd.read_csv(path)

        del df['target']

        categorical_columns = [c for c in df.columns if df[c].dtype.name == 'object']
        numerical_columns = [c for c in df.columns if df[c].dtype.name != 'object']
        t = []
        for column in df.columns:
            if column in categorical_columns:
                t.append((column, True))
            elif column in numerical_columns:
                t.append((column, False))

        return render(request, 'learning_models/learn_model.html', {
            'columns': t,
        })
    elif request.method == 'POST':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).filter(id=model_id)
        if len(learning_models) == 0:
            return JsonResponse({'nothing': 'nothing'})

        data = deepcopy(request.POST)
        param = {}
        param['iterations'] = int(data['iterations'])
        data.pop('iterations')
        param['learning_rate'] = float(data['learning_rate'])
        data.pop('learning_rate')
        param['depth'] = int(data['depth'])
        data.pop('depth')
        param['l2_leaf_reg'] = float(data['l2_leaf_reg'])
        data.pop('l2_leaf_reg')
        param['rsm'] = float(data['rsm'])
        data.pop('rsm')
        param['nan_mode'] = data['nan_mode']
        data.pop('nan_mode')
        # param['has_time'] = bool(data['has_time'])
        # data.pop('has_time')
        param['one_hot_max_size'] = int(data['one_hot_max_size'])
        data.pop('one_hot_max_size')
        param['bagging_temperature'] = float(data['bagging_temperature'])
        data.pop('bagging_temperature')
        param['min_data_in_leaf'] = int(data['min_data_in_leaf'])
        data.pop('min_data_in_leaf')
        param['max_leaves'] = int(data['max_leaves'])
        data.pop('max_leaves')
        # if 'cross_validation' in data:
            # cross_validation = bool(data['cross_validation'])
            # folds = int(data['folds'])
            # folds = 5
            # data.pop('cross_validation')
        # else:
            # cross_validation = False
            # folds = 0
        cross_validation = False
        # data.pop('folds')
        data.pop('csrfmiddlewaretoken')
        # Добавить в параметры
        param['cat_features'] = []

        for key in data:
            param['cat_features'].append(key)

        for learning_model in learning_models:
            model = learning_model
            path = learning_model.path
            name = learning_model.name

        # cat_features2 = ['sex', 'cp', 'fbs', 'restecg', 'exng', 'slp', 'caa', 'thall']
        # param2 = {
        #     'cat_features': cat_features2,
        #     'iterations': 1000,
        #     'learning_rate': 0.1,
        #     'l2_leaf_reg': 1,
        #     'rsm': 0.6,
        # }

        if cross_validation:
            result = fit_model_cv(name, path, param, folds)
        else:
            result = fit_model(name, path, param)

        model.is_learned = True
        model.save()

        graph_data_learn = []

        with open(f'{name}/learn_error.tsv', 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                graph_data_learn.append(row)

            graph_data_learn = graph_data_learn[1:]

            graph_data_learn_int = []

            for item in graph_data_learn:
                graph_data_learn_int.append((int(item[0]), float(item[1])))


        graph_data_learn = []
        with open(f'{name}/test_error.tsv', 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                graph_data_learn.append(row)

            graph_data_learn = graph_data_learn[1:]

            graph_test_learn_int = []

            for item in graph_data_learn:
                graph_test_learn_int.append((int(item[0]), float(item[1])))

        graph_data_learn_cv = []
        graph_data_learn_cv_add = []
        cvs_mean_learn = []

        if cross_validation:
            for i in range(1,6):
                graph_data_learn_small = []
                with open(f'{name}_cv_{i}/learn_error.tsv', 'r', newline='') as csvfile:
                    reader = csv.reader(csvfile, delimiter='\t')
                    for row in reader:
                        if row!=['iter', 'Logloss']:
                            row.extend([i])
                            graph_data_learn_cv_add.append((int(row[0]), float(row[1]), int(row[2])))
                            graph_data_learn_small.append((int(row[0]), float(row[1]), int(row[2])))
                graph_data_learn_cv.append(graph_data_learn_small)

            df_graph_data_learn = pd.DataFrame(graph_data_learn_cv_add,columns=['iter', 'Logloss','fold'])
            df_graph_data_learn['Logloss'] = pd.to_numeric(df_graph_data_learn['Logloss'])
            df_graph_data_learn['Logloss_for_mean']=df_graph_data_learn['Logloss']
            df_graph_data_learn['Logloss_for_std']=df_graph_data_learn['Logloss']
            df_graph_data_learn_mean_std = df_graph_data_learn[['iter','Logloss_for_mean','Logloss_for_std']].groupby(['iter']).agg({'Logloss_for_mean':'mean','Logloss_for_std':'std'}).reset_index()
            cvs_mean_learn = df_graph_data_learn_mean_std.values.tolist()

            graph_data_test = []

            with open(f'{name}/test_error.tsv', 'r', newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter='\t')
                for row in reader:
                    graph_data_test.append(row)

                graph_data_test = graph_data_test[1:]

                graph_data_test_int = []

                for item in graph_data_test:
                    graph_data_test_int.append((int(item[0]), float(item[1])))

            graph_data_test_cv = []
            graph_data_test_cv_add = []
            cvs_mean_test = []

            for i in range(1,6):
                graph_data_test_small = []
                with open(f'{name}_cv_{i}/test_error.tsv', 'r', newline='') as csvfile:
                    reader = csv.reader(csvfile, delimiter='\t')
                    for row in reader:
                        if row!=['iter', 'Logloss']:
                            row.extend([i])
                            graph_data_test_cv_add.append((int(row[0]), float(row[1]), int(row[2])))
                            graph_data_test_small.append((int(row[0]), float(row[1]), int(row[2])))
                graph_data_test_cv.append(graph_data_test_small)

            df_graph_data_test = pd.DataFrame(graph_data_test_cv_add,columns=['iter', 'Logloss','fold'])
            df_graph_data_test['Logloss'] = pd.to_numeric(df_graph_data_test['Logloss'])
            df_graph_data_test['Logloss_for_mean']=df_graph_data_test['Logloss']
            df_graph_data_test['Logloss_for_std']=df_graph_data_test['Logloss']
            df_graph_data_test_mean_std = df_graph_data_test[['iter','Logloss_for_mean','Logloss_for_std']].groupby(['iter']).agg({'Logloss_for_mean':'mean','Logloss_for_std':'std'}).reset_index()
            cvs_mean_test = df_graph_data_test_mean_std.values.tolist()

            path_cv = shap_summary_cv(name)
            path = shap_summary(name)

            res = info_cv(name)

            x = res[0]
            x['value_model'] = x['value_model'].round(3)
            x['value_cv'] = x['value_cv'].round(3)

            table = x.values.tolist()
            x = res[1]
            x['value_model'] = x['value_model'].round(3)
            x['value_cv'] = x['value_cv'].round(3)
            table2 = x.values.tolist()
            params = res[2]
            x = shap_importance_cv(name)
            x['value'] = x['value'].round(3)
            x['value_cv'] = x['value_cv'].round(3)
            table3 = x.values.tolist()



            return render(request, 'learning_models/learn_result_cv.html', {
                'msg': result,
                'graph_data_learn': json.dumps(graph_data_learn_int),
                'cvs_learn': json.dumps(graph_data_learn_cv),
                'cvs_mean_learn': json.dumps(cvs_mean_learn),
                'graph_data_test': json.dumps(graph_data_test_int),
                'cvs_test': json.dumps(graph_data_test_cv),
                'cvs_mean_test': json.dumps(cvs_mean_test),
                'path_cv': path_cv,
                'path': path,
                'table': table,
                'table2': table2,
                'table3': table3,
                'params': params,
            })

        return render(request, 'learning_models/learn_result.html', {
            'msg': result,
            'graph_data_learn': json.dumps(graph_data_learn_int),
            'graph_test_learn': json.dumps(graph_test_learn_int),
            'cvs_learn': json.dumps(graph_data_learn_cv),
            'cvs_mean_learn': json.dumps(cvs_mean_learn),
        })

    else:
        HttpResponseNotAllowed(['GET', 'POST'])

@login_required
@csrf_exempt
def get_model_info(request, model_id):
    if request.method == 'GET':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).filter(id=model_id)
        if len(learning_models) == 0:
            return HttpResponseRedirect({'nothing': 'nothing'})
        for learning_model in learning_models:
            model = learning_model

        model_dict = model_to_dict(model)
        graph_data = []
        with open(f'{model.name}/learn_error.tsv', 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                graph_data.append(row)

            graph_data = graph_data[1:]

            graph_data_int = []

            for item in graph_data:
                graph_data_int.append((int(item[0]), float(item[1])))

        graph_data = []
        with open(f'{model.name}/test_error.tsv', 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                graph_data.append(row)

            graph_data = graph_data[1:]

            graph_test_int = []

            for item in graph_data:
                graph_test_int.append((int(item[0]), float(item[1])))

        res = info(model.name)

        text_info = res[:-3]
        params = res[-1]
        table = res[-3].values.tolist()
        table2 = res[-2].values.tolist()

        path = interaction(model.name)

        path2 = shap_summary(model.name)

        table3 = shap_importance(model.name)

        return render(request, 'learning_models/info_model.html', {
            'model': model_dict,
            'graph_data': json.dumps(graph_data_int),
            'graph_data_test': json.dumps(graph_test_int),
            'text_info': text_info,
            'params': params,
            'table': table,
            'table2': table2,
            'table3': table3,
            'path': path,
            'path2': path2,
        })

    else:
        HttpResponseNotAllowed(['GET'])

@login_required
def tree(request, model_id):
    if request.method == 'GET':
        user = request.user
        learning_models = LearningModel.objects.filter(user=user).filter(id=model_id)
        if len(learning_models) == 0:
            return HttpResponseRedirect({'nothing': 'nothing'})
        for learning_model in learning_models:
            model = learning_model

        tree_str = request.GET.get('tree', '')
        value_fields_str = request.GET.get('valueFields', '')

        if tree_str == '':
            return JsonResponse({'error': 'error'})

        if value_fields_str == '':
            return JsonResponse({'error': 'error'})

        value_fields = json.loads(value_fields_str)


        tree = int(tree_str)
        print('view.py value_fields', value_fields,type(value_fields))
        res = predict_some_trees(model.name, tree, tree + 1, value_fields)
        path = show_tree(model.name, tree)

        probability = list(res[0])
        class1 = str(res[1])
        raw = str(res[2])
        table = explain_one_tree_shap(model.name, tree)

        return JsonResponse({
            'probability': probability,
            'class': class1,
            'raw': raw,
            'path': path,
            'table': json.dumps(table),
        })

    else:
        HttpResponseNotAllowed(['GET'])

@login_required
def optimization_model(request):
    if request.method == 'GET':
        x = pd.read_csv('df_iterations_train.csv')
        graph_data_json = x.to_json()

        graph_data_json2 = pd.read_csv('df_iterations_test.csv').to_json()
        graph_data_json3 = pd.read_csv('df_learning_rate_train.csv').to_json()
        graph_data_json4 = pd.read_csv('df_learning_rate_test.csv').to_json()
        graph_data_json5 = pd.read_csv('df_l2_leaf_reg_train.csv').to_json()
        graph_data_json6 = pd.read_csv('df_l2_leaf_reg_test.csv').to_json()
        graph_data_json7 = pd.read_csv('df_random_strength_train.csv').to_json()
        graph_data_json8 = pd.read_csv('df_random_strength_test.csv').to_json()
        graph_data_json9 = pd.read_csv('df_rsm_train.csv').to_json()
        graph_data_json10 = pd.read_csv('df_rsm_test.csv').to_json()


        return render(request, 'learning_models/optimization_model.html', {
            'graph_data': graph_data_json,
            'graph_data2': graph_data_json2,
            'graph_data3': graph_data_json3,
            'graph_data4': graph_data_json4,
            'graph_data5': graph_data_json5,
            'graph_data6': graph_data_json6,
            'graph_data7': graph_data_json7,
            'graph_data8': graph_data_json8,
            'graph_data9': graph_data_json9,
            'graph_data10': graph_data_json10,
        })
    else:
        HttpResponseNotAllowed(['GET'])
